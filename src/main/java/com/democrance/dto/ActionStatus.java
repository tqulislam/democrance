package com.democrance.dto;

import java.util.Map;

public class ActionStatus {

	private Boolean success;
	private String description;
	private String errorCode;
	private Map<String, String> additionalInfos;

	public ActionStatus() {

	}

	public ActionStatus(Boolean success, String description, String errorCode) {
		super();
		this.success = success;
		this.description = description;
		this.errorCode = errorCode;
	}
	
	public ActionStatus(Boolean success, String description, String errorCode, Map<String, String> additionalInfos) {
		super();
		this.success = success;
		this.description = description;
		this.errorCode = errorCode;
		this.additionalInfos= additionalInfos;
	}

	public Boolean getSuccess() {
		return success;
	}

	public void setSuccess(Boolean success) {
		this.success = success;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}

	public Map<String, String> getAdditionalInfos() {
		return additionalInfos;
	}

	public void setAdditionalInfos(Map<String, String> additionalInfos) {
		this.additionalInfos = additionalInfos;
	}

}
