package com.democrance;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemocranceAssignmentApplication {

	public static void main(String[] args) {
		SpringApplication.run(DemocranceAssignmentApplication.class, args);	
	}

}
