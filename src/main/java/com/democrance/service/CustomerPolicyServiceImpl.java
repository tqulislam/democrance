package com.democrance.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.democrance.dto.ActionStatus;
import com.democrance.dto.CustomerPolicyDto;
import com.democrance.model.CustomerPolicy;
import com.democrance.repository.CustomerPolicyRepository;
import com.democrance.repository.CustomerRepository;

@Component
public class CustomerPolicyServiceImpl implements BaseService<CustomerPolicyDto> {

	Logger logger = LoggerFactory.getLogger(CustomerPolicyServiceImpl.class);

	@Autowired
	private CustomerRepository customerRepository;

	@Autowired
	private CustomerPolicyRepository customerPolicyRepository;

	@Override
	public ActionStatus create(CustomerPolicyDto obj) {
		try {
			CustomerPolicy repoObj = new CustomerPolicy(obj.getType(), obj.getPremium(), obj.getCover(),
					getCustomerRepository().getOne(obj.getCustomer().getId()));
			repoObj = getCustomerPolicyRepository().save(repoObj);
			if (repoObj.getId() != null) {
				Map<String, String> additionalInfos = new HashMap<String, String>();
				additionalInfos.put("customerPolicyId", repoObj.getId().toString());
				return new ActionStatus(true, "Success", "0", additionalInfos);
			} else {
				return new ActionStatus(true, "Failure", "1");
			}
		} catch (Exception ex) {
			ex.printStackTrace();
			return new ActionStatus(false, ex.getMessage(), "1");
		}
	}

	@Override
	public ActionStatus update(Long id, CustomerPolicyDto obj) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ActionStatus delete(Long id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public CustomerPolicyDto getById(Long id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<CustomerPolicyDto> getAll() {
		// TODO Auto-generated method stub
		return null;
	}

	public CustomerRepository getCustomerRepository() {
		return customerRepository;
	}

	public void setCustomerRepository(CustomerRepository customerRepository) {
		this.customerRepository = customerRepository;
	}

	public CustomerPolicyRepository getCustomerPolicyRepository() {
		return customerPolicyRepository;
	}

	public void setCustomerPolicyRepository(CustomerPolicyRepository customerPolicyRepository) {
		this.customerPolicyRepository = customerPolicyRepository;
	}
}
