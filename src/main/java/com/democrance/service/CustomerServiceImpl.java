package com.democrance.service;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.democrance.dto.ActionStatus;
import com.democrance.dto.CustomerDto;
import com.democrance.model.Customer;
import com.democrance.repository.CustomerRepository;

@Component
public class CustomerServiceImpl implements BaseService<CustomerDto> {

	Logger logger = LoggerFactory.getLogger(CustomerServiceImpl.class);

	@Autowired
	private CustomerRepository customerRepository;

	@Override
	public ActionStatus create(CustomerDto obj) {
		try {
			Date dob = new SimpleDateFormat("dd-MM-yyyy").parse(obj.getDob());
			Customer repoObj = new Customer(obj.getFirst_name(), obj.getLast_name(), dob);
			repoObj = getCustomerRepository().save(repoObj);
			if (repoObj.getId() != null) {
				Map<String, String> additionalInfos = new HashMap<String, String>();
				additionalInfos.put("customerId", repoObj.getId().toString());
				return new ActionStatus(true, "Success", "0", additionalInfos);
			} else {
				return new ActionStatus(true, "Failure", "1");
			}
		} catch (Exception ex) {
			ex.printStackTrace();
			return new ActionStatus(false, ex.getMessage(), "1");
		}
	}

	@Override
	public ActionStatus update(Long id, CustomerDto obj) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ActionStatus delete(Long id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public CustomerDto getById(Long id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<CustomerDto> getAll() {
		// TODO Auto-generated method stub
		return null;
	}

	public CustomerRepository getCustomerRepository() {
		return customerRepository;
	}

	public void setCustomerRepository(CustomerRepository customerRepository) {
		this.customerRepository = customerRepository;
	}
}
