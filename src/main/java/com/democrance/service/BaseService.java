package com.democrance.service;

import java.util.List;

import com.democrance.dto.ActionStatus;

public interface BaseService<T> {
	public abstract ActionStatus create(T obj);

	public abstract ActionStatus update(Long id, T obj);

	public abstract ActionStatus delete(Long id);

	public abstract T getById(Long id);

	public abstract List<T> getAll();
}
