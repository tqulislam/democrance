package com.democrance.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.democrance.model.Customer;

@RepositoryRestResource(path = "customer")
public interface CustomerRepository extends JpaRepository<Customer, Long> {

}
