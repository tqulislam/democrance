package com.democrance.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.democrance.model.CustomerPolicy;

@RepositoryRestResource(path = "customer")
public interface CustomerPolicyRepository extends JpaRepository<CustomerPolicy, Long> {

}
