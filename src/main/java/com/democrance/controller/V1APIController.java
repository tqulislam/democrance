package com.democrance.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.democrance.dto.ActionStatus;
import com.democrance.dto.CustomerDto;
import com.democrance.dto.CustomerPolicyDto;
import com.democrance.service.CustomerPolicyServiceImpl;
import com.democrance.service.CustomerServiceImpl;

@RestController
@RequestMapping("/v1")
public class V1APIController {

	@Autowired
	private CustomerServiceImpl customerService;

	@Autowired
	private CustomerPolicyServiceImpl customerPolicyService;

	@PostMapping("/create_customer")
	public ResponseEntity<ActionStatus> createCustomer(@RequestBody CustomerDto request) {
		ActionStatus response = getCustomerService().create(request);
		return ResponseEntity.ok(response);
	}

	@PostMapping("/create_policy")
	public ResponseEntity<ActionStatus> createCustomerPolicy(@RequestBody CustomerPolicyDto request) {
		ActionStatus response = getCustomerPolicyService().create(request);
		return ResponseEntity.ok(response);
	}

	public CustomerServiceImpl getCustomerService() {
		return customerService;
	}

	public void setCustomerService(CustomerServiceImpl customerService) {
		this.customerService = customerService;
	}

	public CustomerPolicyServiceImpl getCustomerPolicyService() {
		return customerPolicyService;
	}

	public void setCustomerPolicyService(CustomerPolicyServiceImpl customerPolicyService) {
		this.customerPolicyService = customerPolicyService;
	}

}