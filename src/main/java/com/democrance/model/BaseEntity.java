
package com.democrance.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

@MappedSuperclass
public class BaseEntity {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;

	@Column(name = "creation_date", nullable = true)
	private Date creationDate;

	@Column(name = "modified_date", nullable = true)
	private Date modifiedDate;

	@Column(name = "is_deleted", nullable = true)
	private boolean isDeleted;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public Date getModifiedDate() {
		return modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	public boolean isDeleted() {
		return isDeleted;
	}

	public void setDeleted(boolean isDeleted) {
		this.isDeleted = isDeleted;
	}

	public BaseEntity() {
	}

	public BaseEntity(Long id, Date creationDate, Date modifiedDate, boolean isDeleted) {
		super();
		this.id = id;
		this.creationDate = creationDate;
		this.modifiedDate = modifiedDate;
		this.isDeleted = isDeleted;
	}

}