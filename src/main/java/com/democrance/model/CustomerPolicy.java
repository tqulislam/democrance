package com.democrance.model;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity(name = "CustomerPolicy")
@Table(name = "customer_policy")
public class CustomerPolicy extends BaseEntity {

	@ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@JoinColumn(name = "customer_id", nullable = false)
	private Customer customer;

	@Column(name = "first_name", nullable = false)
	private String type;

	@Column(name = "premium", nullable = false)
	private Integer premium;

	@Column(name = "cover", nullable = true)
	private Integer cover;

	public CustomerPolicy() {
	}

	public CustomerPolicy(String type, Integer premium, Integer cover, Customer customer) {
		super();
		this.type = type;
		this.premium = premium;
		this.cover = cover;
		this.customer = customer;
		this.setCreationDate(new Date());
		this.setDeleted(false);
		this.setModifiedDate(null);
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Integer getPremium() {
		return premium;
	}

	public void setPremium(Integer premium) {
		this.premium = premium;
	}

	public Integer getCover() {
		return cover;
	}

	public void setCover(Integer cover) {
		this.cover = cover;
	}

}